function CollectionProperty(name, elementType) {
	this.Name = name;
	this.Type = 'Collection';
	this.Description = 'description';
	this.ElementType = elementType;
}


module.exports = {
	getComplexProperty: function(name, type, properties) {
		var complexProperty = {
			Description: name ? name + ' information': 'element information',
			Type: type,
			Annotations: []
		};

		if (name) {
			complexProperty['Name'] = name;
		}

		if (properties) {
			complexProperty['Properties'] = properties;
		}

		return complexProperty;
	},

	getCollectionProperty: function(name, elementType) {
		return new CollectionProperty(name, elementType);
	}
};