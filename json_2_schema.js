function Json2Schema() {
	var _ = require('lodash');
	var pg = require('./schema_properties');

	var isInt = function (n) {
	   return n % 1 === 0;
	};

	var getComplexPropertySchema = function(propName, type, prop) {
		var subPropertyNames = _.keys(prop);
		var subPropertySchemas = [];

		_.forEach(subPropertyNames, function(subPropertyName) {
			var subProperty = prop[subPropertyName];
			var subPropertySchema = getPropertySchema(subPropertyName, subProperty);
			subPropertySchemas.push(subPropertySchema);
		});

		var propertySchema = pg.getComplexProperty(propName, type, subPropertySchemas);
		return propertySchema;
	};

	var getCollectionPropertySchema = function(propName, collectionProp) {
		if (collectionProp.length === 0) {
			return null;
		}

		var propertyInstance = collectionProp[0];
		var type = getType(propertyInstance);
		var elementTypeSchema = getComplexPropertySchema(null, type, propertyInstance);

		return pg.getCollectionProperty(propName, elementTypeSchema);
	};


	var getPropertySchema = function(propName, prop) {
		var type = getType(prop);

		if (type === 'collection') {
			return getCollectionPropertySchema(propName, prop);
		}

		if (type === 'object') {
			return getComplexPropertySchema(propName, type, prop);
		}

		//base condition: primitive type
		return pg.getComplexProperty(propName, type);
	};
	

	var getType = function(prop) {
		var propType = typeof prop;

		if (propType === 'number') {
			if (isInt(prop)) {
				return 'int';
			} 

			return 'float';
		}

		if (propType === 'object') {
			if (_.isArray(prop)) {
				return 'collection';
			}

			return 'object';
		}

		return propType;
	};

	return {
		getSchema: function(jsonObj) {
			var schema = {
				"Name": "Name",
				"BaseType": "Entity",
				"DisplayName": "Display Name",
				"Description": "Some descriptions",
			};

			var propertyNames = _.keys(jsonObj);
			var properties = [];
			_.forEach(propertyNames, function(propertyName) {
				var property = jsonObj[propertyName];
				var propertySchema = getPropertySchema(propertyName, property);
				// schema[propertyName] = propertySchema;

				properties.push(propertySchema);
			});

			schema['Properties'] = properties;
			return schema;
		}
	};
}


module.exports = new Json2Schema();