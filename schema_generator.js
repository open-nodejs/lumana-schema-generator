var logger = require('./log/logger');
var fs = require('fs');
var path = require('path');
var _ = require('lodash');
var schemaGenerator = require('./json_2_schema');


var args = process.argv;
var relativeFilePath = args[2];
var filepath = path.join(__dirname, relativeFilePath);
// var content = fs.readFileSync('./data/server-records.json', 'utf8');
// var content = fs.readFileSync('./data/storage-array-records.json', 'utf8');

var content = fs.readFileSync(filepath, 'utf8');
var contentInJson = JSON.parse(content);

// var type = typeof contentInJson.system;
// logger.print(schema.generateSchema(contentInJson));
var schema = schemaGenerator.getSchema(contentInJson);
logger.print(JSON.stringify(schema, null, 4));