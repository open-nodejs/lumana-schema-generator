# Prerequisite
Install latest `node`. This script is developed in Node v7.5.0

# How to run
Run `npm install` to download dependencies.
Run `node schema_generator.js ./data/<filename>`